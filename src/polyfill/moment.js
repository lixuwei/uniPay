// moment().format('YYYY-MM-DD HH:mm:ss'),
// 仅被alipay-sdk使用
import {
  getOffsetDate,
  getFullTimeStr
} from '../shared/utils'

export default function () {
  return {
    format: function () {
      return getFullTimeStr(getOffsetDate(8))
    }
  }
}
